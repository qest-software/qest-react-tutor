/**
 * Created by radims on 10.08.16.
 */
import React, { Component } from 'react';


export default class App extends Component {

    isData(){
        return this.props.town && this.props.town.main;
    }

    renderNothing(){
        return (<div>Zadejte nejdříve nějaké město</div>);
    }

    render() {
        if(!this.isData()) {
            return this.renderNothing();
        }
        return (
            <div>
                <h1>Nalezené město</h1>
                {this.props.town ? this.props.town.name: ""}
                <h2>Má teplotu</h2>
                {this.props.town ? this.props.town.main.temp: ""} °C
                <h2>Tlak</h2>
                {this.props.town ? this.props.town.main.pressure: ""}
            </div>
        );
    }
}
/**
 * Created by radims on 10.08.16.
 */

