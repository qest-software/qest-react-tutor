import React, { Component } from 'react';
import Title from "./title";
import CurrentTown from "./current_town";
import Town from "./town";

import axios from "axios";

const API_KEY = "b33dfd61e84f168076c06c51261b214a";
const API_URL = `http://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}&units=metric`;
const DEFAULT_WHEATER = "czech+republic";


export default class App extends Component {

    //Puštěno při načtení dané komponenty
    componentWillMount() {
        //nastavení počátečního stavu aplikace
        this.state = {
            currentWord: "",
            townList: []
        };
    }

    onAddClick() {
        this.setState({
            townList: this.state.townList
                .filter((item)=>{return this.state.town && (item.id != this.state.town.id)})
                .concat([this.state.town])
        });
    }

    inputOnChange(event) {
        axios.get(`${API_URL}&q=${event.target.value}`).then((response)=> {
            this.setState({
                town: response.data
            })
        });
        //změna stavu aplikace
        this.setState({
            currentWord: event.target.value
        })
    }

    onTownDelete(item){
        this.setState({
            townList: this.state.townList.filter((town)=>{
                return item.id != town.id;
            })
        })
    }


    render() {
        return (
            <div className="container">
                <input type="text" onChange={this.inputOnChange.bind(this)}/>
                <button className="btn btn-success" onClick={this.onAddClick.bind(this)}>+</button>
                Je tam napsané slov: {this.state.currentWord}
                <Title title={this.state.currentWord}/>
                <CurrentTown town={this.state.town}/>
                {this.state.townList.map((item)=> {
                    return (
                        <Town key={item.id} town={item} onDelete={()=>{this.onTownDelete(item)}}/>
                    )
                })}
            </div>
        );
    }
}
