/**
 * Created by radims on 10.08.16.
 */
import React, { Component } from 'react';


export default class App extends Component {

    isData(){
        return this.props.town && this.props.town.main;
    }

    renderNothing(){
        return (<div>Zadejte nejdříve nějaké město</div>);
    }

    render() {
        if(!this.isData()) {
            return this.renderNothing();
        }
        return (
            <div>
                <hr/>
                <p>Nalezené město</p>
                {this.props.town ? this.props.town.name: ""}
                <p>Má teplotu</p>
                {this.props.town ? this.props.town.main.temp: ""} °C
                <p>Tlak</p>
                {this.props.town ? this.props.town.main.pressure: ""}
                <button className="btn btn-default" onClick={this.props.onDelete}>smazat</button>
            </div>
        );
    }
}
/**
 * Created by radims on 10.08.16.
 */

