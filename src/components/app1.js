import React, { Component } from 'react';
import axios from "axios";

const API_KEY = "b33dfd61e84f168076c06c51261b214a";
const API_URL = `http://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}&units=metric`;
const DEFAULT_WHEATER = "czech+republic";


export default class App extends Component {

    isCityFound(reponse) {
        return reponse.data.cod == 200;
    }

    reloadWeather(query) {
        axios.get(`${API_URL}&q=${query}`).then((response) => {
            this.setState({
                current: this.isCityFound(response) ? response.data : null
            })
        })
    }

    componentWillMount() {
        this.state = {
            searchWord: "",
            current: null,
            lastSearch: []
        };
        this.reloadWeather(DEFAULT_WHEATER);
    }

    inputOnChange(event) {
        this.setState({
            searchWord: event.target.value
        });
        this.reloadWeather(event.target.value);
    }

    onRemoveClick(item){
        this.setState({
            ...this.state,
            lastSearch: this.state.lastSearch.filter((stateItem)=>{
                if(item.id != stateItem.id) {
                    return stateItem;
                }
            })
        });
    }


    addButtonClicked() {
        this.setState({
            lastSearch: this.state.lastSearch.concat([this.state.current])
        })
    }

    render() {
        return (
            <div>
                <input type="text" name="searchBar" onChange={this.inputOnChange.bind(this)}/>
                <button onClick={this.addButtonClicked.bind(this)}>Add</button>
                <h1>{this.state.searchWord}</h1>
                <h2>
                    {this.state.current ? this.state.current.name : null}:
                    {this.state.current ? this.state.current.main.temp : null} °C
                </h2>
                {this.state.lastSearch.map((item)=> {
                    return (
                        <li key={item.id}>
                            {item.name} ({item.sys.country}) - {item.main.temp}
                            <button className="fa fa-icon" onClick={(event)=>{this.onRemoveClick(item)}}>X</button>
                        </li>
                    );
                })}
            </div>
        );
    }
}
